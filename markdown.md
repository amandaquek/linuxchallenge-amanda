# Question 1: Problem Management Exercise

>*For each of the scenarios below suggest:*
>
>* A quick fix
>
>* One or more possible causes
>
>* A change or other action to remove the root cause

>*1. Three mornings in a row your car battery is dead.*

## Answer for part 1:

* Borrow a jump starter from a friend for the time being
* Go to the nearest car workshop
* Call for tow car

## Answer for part 2

* Car was left turned on without starting the engine
* Car battery died due to wear and tear

## Answer for part 3

* Change a new battery for the car

>*2. Users in the Marketing department report the system locks up when doing a monthly copy of data from their Access database to the excel spreadsheets they use to produce monthly statistics on sales.*


## Answer for part 1

* Export data to excel instead of copying to excel
* Copy data onto the clipboard and paste it onto the Excel spreadsheet

## Answer for part 2

* The marketing department is not aware that they have to paste their data into a clipboard before pasting it in excel.

## Answer for part 3

* Educate the marketing department by conducting a workshop/sending out an email so that they know that they should export the Access database into an excel file to retrieve the data.


>*3. Users are frequently locked out of their system because they forget their passwords when returning from periods of leave.*

## Answer for part 1

* Get user to choose and answer a question that correspond to the password that they have set. Using it as a prompter when user forgets their password

## Answer for part 2

* Password requirement is too complex
* Password set by user is not frequently used
* System requires password change too frequently

## Answer for part 3

* Implement an OTP instead

# Question 2: ITSM as a Practice / Strategy Exercise

>*Working in a group, define the Utility & Warranty aspects of the business services that are (or could be) provided by a bank to its customers via its ATM network.*
> 
>*Prepare a brief presentation on flip charts for delivery to the rest of the course.*

  

## Background

Utility and warranty are value propositions of service strategies.

  

First, let’s relook at what utility and warranty is defined as:

* Utility
* Supporting business outcomes in terms of enhancing or enabling the performance of customer assets
* Warranty
* Providing assurance in terms of availability, capacity, security and continuity

Now, let’s look at what an ATM network is:

* An automated teller machine (ATM) is an electronic banking outlet that allows customers to complete basic transactions without the aid of a branch representative or teller. Anyone with a credit card or debit card can access most ATMs

## Answer

**Utility Value Propositions**
* Enhancing performance of customer assets
* Pay bills to certain companies
* Activate iBanking
* Pay for income tax
* Enabling performance of customer assets
* Allowing customers to retrieve money from their bank accounts instantly
* Performing cashcard top-up on the ATM

**Warranty Value Propositions**
* Availability
* It is up and running 24/7
* Machines located all around the country
* Capacity
* Multiple machines are available to allow each and every client in the bank to access.
* Security
* CCTV
* Pinguard shield protector to cover the number buttons when entering the pin
* Mirror to see if anyone is looking at the ATM screen from behind.
* ATM and the connection to the bank is using a secure network.
* Continuity
* In case of downtime of the ATM, there should be another ATM in close proximity to provide service to the customers.


# Question 3: Design Exercise

>*A Service Level Agreement is to be established between IT Services and the business unit responsible for the for the ATM services. A meeting has been called to discuss the contents and you have been asked to prepare some notes beforehand.*
> 
>*In your groups, identify the key items that should be included in such an SLA.*
> 
>*Your lecturer will advise you of whether to take the role of IT Services or the business unit.*
> 
>*Prepare a brief presentation of your thoughts.*

## Answer (IT Services)
* The ATM Availability downtime should be not more than 2%
* Withdrawal transaction should not last more than 10 seconds
* Amount disbursed from the machine should be 100% accurate
* Deduction of withdrawal amount should be updated accurately
* Basic functions such as cash withdrawal, funds transfer and bill payments should be available to client

# Question 4: Service Transition exercise

>*It is proposed to install a new version of the software that provides the application functionality for the ATM system. Identify the key members of a CAB meeting called to discuss the proposed change.*
>
>*List the roles on a flipchart.*

## Answer
* ATM engineers
* Staff / Vendors that developed the new software
* Change Manager
* Person in charge of the whole change
* Simulated user group
* People in charge of testing the system and giving feedback during trial
* Service Desk Analyst/Manager
* Staff in charge of offering insights to common issues with service delivery and troubleshooting
* Operations Manager
* Manager in charge of day-to-day running of the application
* Information Security Officer
* Managers in charge of day-to-day aspects of network security
* Business Relationship Managers
* RMs that work directly with clients and are able to better identify client wants and needs

# Question 5: CSI Exercise
>*Identify some of the key roles that would provide input for seeking improvement in the customer service provided by the ATM system, and the types of input that they could provide.*
>
>*List the roles and input on a flipchart.*

## Answer
* Customers of the bank who are frequent users of the ATM
* Types of input: Survey done on customers to understand what the customers feel are lacking in the current ATM system
* The bank’s team (Service Desk Analyst/Manager) who supports the ATM
* Types of input: Understand the common problems faced by customers through complaints/ dial-in calls.
* Bank tellers in branches who face customers on a daily basis.
* Types of input: Choice of visiting the branch instead of ATM
* Operations Manager (Manager in charge of day-to-day running of the application)
* Types of input: identify issues faced by the customers when using ATM services
* Call center staff
* Types of input: List of problems that customers call in to enquire (relating to ATM)