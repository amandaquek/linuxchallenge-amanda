The txt file is easier to work with as there are lesser conditions to check after performing the extraction. However, using csv, the deliminated values falls under multiple column of the extracted result which causes more conditional checking.

The total number of files return different results.
Total number of files extracted from txt = 18261
Total number of files extracted from csv = 18262
